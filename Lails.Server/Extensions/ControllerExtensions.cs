﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace System.Web.Http
{
    public static partial class ControllerExtensions
    {
        public static string GetHeader(this ApiController controller, string name = "appid")
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                name = "appid";
            }
            IEnumerable<string> values = null;
            if (controller.Request.Headers != null && controller.Request.Headers.TryGetValues(name, out values) && values != null && values.Count() > 0)
            {
                return values.FirstOrDefault();
            }
            return null;
        }
    }
}
